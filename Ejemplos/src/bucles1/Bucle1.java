package bucles1;

import java.util.Scanner;

public class Bucle1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int numero  = input.nextInt();
		input.nextLine();
		
		for(int i = 0; i < numero ; i++) {
			String cadena = input.nextLine();
			
			if(cadena.contains("a") && cadena.contains("e") && cadena.contains("i") 
					&& cadena.contains("o") && cadena.contains("u")) {

				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
		}
		
		input.close();
	}

}
