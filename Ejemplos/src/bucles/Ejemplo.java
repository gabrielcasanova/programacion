package bucles;

import java.util.Scanner;

public class Ejemplo {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int contadorMinusculas = 0;
		int contadorMayusculas = 0;
		int contadorCifras = 0;
		int contadorRestoCaracteres = 0;
		int sumaDeCifras = 0;
		String cadena;
		
		do {
			System.out.println("Introduce una cadena");
			cadena = input.nextLine();

			// Mostrar sus caracteres en una linea distinta
			for (int i = 0; i < cadena.length(); i++) {
				char caracter = cadena.charAt(i);

				if (caracter >= 'a' && caracter <= 'z') {
					contadorMinusculas++;
				} else if (caracter >= 'A' && caracter <= 'Z') {
					contadorMayusculas++;
				} else if (caracter >= '0' && caracter <= '9') {
					contadorCifras++;
					// Sumar valores de las cifras
					sumaDeCifras = sumaDeCifras + (caracter - '0');
				} else {
					contadorRestoCaracteres++;
				}
			}
				
		} while ( cadena.equals("fin") );

		System.out.println("Cantidad minusculas: " + contadorMinusculas);
		System.out.println("Cantidad mayusculas: " + contadorMayusculas);
		System.out.println("Cantidad cifras: " + contadorCifras);
		System.out.println("Cantidad resto de caracteres: " + contadorRestoCaracteres);
		System.out.println("La suma total de cifras es: " + sumaDeCifras);

		
		input.close();
	}

}
