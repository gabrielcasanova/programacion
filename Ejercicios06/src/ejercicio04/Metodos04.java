package ejercicio04;

public class Metodos04 {
	
	static boolean esPrimo(int numero){
		
		//Si me encuentro un divisor ya no es primo
		for(int i = 2 ; i < numero; i++){
			if(numero % i == 0){
				return false;
			}
		}
		
		return true;
	}
}
