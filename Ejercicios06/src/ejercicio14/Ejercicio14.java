package ejercicio14;

import ejercicio10.Ejercicio10;

public class Ejercicio14 {

	public static void main(String[] args) {
		
		//Integer.parseInt("-234");
		int numero = deCadenaAEntero("374644");
		int numero2 = deCadenaAEntero("2-4");
		
		System.out.println(numero);
		System.out.println(numero2);
	}

	private static int deCadenaAEntero(String cadena) {
		int resultado = 0;
		
		if(Ejercicio10.esEntero(cadena)){
			//Convierto ese String a int
			int exponente = 0;
			
			// 345
			// -346
			//El for llega hasta el caracter 0, pero no lo evalua
			for(int i = cadena.length() - 1; i > 0; i--){
				resultado = resultado + ((cadena.charAt(i) - 48) * (int)Math.pow(10, exponente));
				exponente++;
			}
			
			if(cadena.charAt(0) == '-'){
				resultado = - resultado;
			}else{
				resultado = resultado + ((cadena.charAt(0) - 48) * (int)Math.pow(10, exponente));
			}
		}

		return resultado;
	}

}
