package ejercicio16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena = input.nextLine();
		
		cadena = invertirCadena(cadena);
		
		System.out.println(cadena);
		
		input.close();
	}

	static String invertirCadena(String cadena) {
		String resultado = "";
		
		for(int i = cadena.length() - 1; i >= 0; i--){
			resultado += cadena.charAt(i);
		}
		
		return resultado;
	}

}
