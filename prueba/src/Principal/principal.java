package Principal;

import java.util.Scanner;

public class principal {
	private final static byte NUM_PALABRAS = 10;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];

	public static void main(String[] args) {

		String palabraSecreta;
		palabras[0]="ORDENADOR";
		palabras[1]="DATABASE";
		palabras[2]="STRING";
		palabras[3]="ATRIBUTO";
		palabras[4]="HERENCIA";
		palabras[5]="CLASE";
		palabras[6]="METODO";
		palabras[7]="OBJETO";
		palabras[8]="EXCEPCION";
		palabras[9]="PRIVADO";

		Scanner input = new Scanner(System.in);

		palabraSecreta = palabras[(int) (Math.random() * NUM_PALABRAS)];

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		String caracteresElegidos = "";
		int fallos;
		boolean acertado;
		System.out.println("Acierta la palabra");
		do {

			System.out.println("####################################");

			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					System.out.print(" -");
				} else {
					System.out.print(" " + caracteresPalabra[0][i]);
				}
			}
			System.out.println();

			System.out.println("Introduce una letra o acierta la palabra");
			System.out.println("Caracteres Elegidos: " + caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			fallos = 0;

			boolean encontrado;
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = false;
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
						encontrado = true;
					}
				}
				if (!encontrado)
					fallos++;
			}

			switch (fallos) {
			case 1:

				System.out.println("     ___");
				break;
			case 2:

				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 3:
				System.out.println("  ____ ");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 4:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println("      |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 5:
				System.out.println("  ____ ");
				System.out.println(" |    |");
				System.out.println(" O    |");
				System.out.println("      |");
				System.out.println("     ___");
				break;
			case 6:
				System.out.println("    ____ ");
				System.out.println("  |     |");
				System.out.println("  O     |");
				System.out.println(" -|-    |");
				System.out.println("       ___");
				break;
			case 7:
				System.out.println("   ____ ");
				System.out.println("  |     |");
				System.out.println("  O     |");
				System.out.println(" -|-    |");
				System.out.println("  A    ___");
				break;
			}

			if (fallos >= FALLOS) {
				System.out.println("Has perdido: " + palabraSecreta);
			}
			acertado = true;
			for (int i = 0; i < caracteresPalabra[1].length; i++) {
				if (caracteresPalabra[1][i] != '1') {
					acertado = false;
					break;
				}
			}
			if (acertado)
				System.out.println("Has Acertado ");

		} while (!acertado && fallos < FALLOS);

		input.close();
	}

}