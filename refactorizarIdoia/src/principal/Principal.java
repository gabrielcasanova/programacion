package principal;
//Lo quito porque no lo necesito en esta clase
//import java.util.Scanner;
//Importo clases necesarias
import clases.Comprobar;
import clases.Escoger;
import clases.Pintar;

public class Principal {
	// No hace falta NUMPALABRAS, puesto que es lo mismo que palabras.length, adem�s
	// le cambio el tipo de dato a int
	// private final static int palabras.length = 10;

	//Son las oportunidades, le cambio el nombre, no hace falta que sea static, lo muevo al main
	// private final static byte OPORTUNIDADES = 7;
	//private static String[] palabras = { "ORDENADOR", "DATABASE", "STRING", "ATRIBUTO", "HERENCIA", "CLASE", "METODO",
	//"OBJETO", "EXCEPCION", "PRIVADO" };

	public static void main(String[] args) {
		// Declaro palabraSecreta donde se vaya a utilizar
		// String palabraSecreta;
		// Coloca las palabras, las inicializo donde se declara el String
//		palabras[0] = "ORDENADOR";
//		palabras[1] = "DATABASE";
//		palabras[2] = "STRING";
//		palabras[3] = "ATRIBUTO";
//		palabras[4] = "HERENCIA";
//		palabras[5] = "CLASE";
//		palabras[6] = "METODO";
//		palabras[7] = "OBJETO";
//		palabras[8] = "EXCEPCION";
//		palabras[9] = "PRIVADO";
		//Lo quito porque solo lo necesito en la clase Escoger
		//Scanner input = new Scanner(System.in);

		//Lo meto en un metodo dentro de la clase Escoger -> escogerPalabraSecreta
		//Escogo una palabra aleatoria del array
		String palabraSecreta = Escoger.palabraSecreta();

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		String caracteresElegidos = "";
		// Lo declaro donde se vaya a utilizar
		int fallos;
		boolean acertado;
		int oportunidades = 7;
		System.out.println("Acierta la palabra");
		do {
			//Pinto caracteres - cambio por metodo pintoCaracteres
			Pintar.pintarCaracteresVacios(caracteresPalabra);

			//Pido caracteres o palabras, lo meto en un metodo dentro de la clase Escoger que se llame escogerCaracteres
			caracteresElegidos = Escoger.escogerCaracteres(caracteresElegidos);
			
			fallos = 0;
			//Lo declaro donde se vaya a utilizar
			//boolean encontrado;
			//Lo meto en un metodo que se llame comprobarCaracteres en una clase que se llame Comprobar
			fallos = Comprobar.comprobarCaracteres(caracteresElegidos, caracteresPalabra, fallos);
			
			//Cambio el switch por metodo pintarAhorcado(int fallos)
			Pintar.pintarAhorcado(fallos);
			
			acertado = true;
			acertado = Comprobar.comprobarGanar(caracteresPalabra, acertado);
		
//			for (int i = 0; i < caracteresPalabra[1].length; i++) {
//				if (caracteresPalabra[1][i] != '1') {
//					acertado = false;
//					break;
//				}
//			}
			
			if (fallos >= oportunidades) {
				System.out.println("Has perdido, la palabra era: " + palabraSecreta);
			}

			// Coloco llaves
			if (acertado) {
				System.out.println("Has Acertado ");
			}

		} while (!acertado && fallos < oportunidades);

	
	}

}
