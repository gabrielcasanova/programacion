package clases;

import java.util.Scanner;

public class Escoger {

	public static String escogerCaracteres(String caracteresElegidos) {
		// Declaro Scanner aqui
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
		caracteresElegidos += input.nextLine().toUpperCase();

		// Devuelvo caracteresElegidos
		return caracteresElegidos;

	}

	public static String palabraSecreta() {
		String[] palabras = { "ORDENADOR", "DATABASE", "STRING", "ATRIBUTO", "HERENCIA", "CLASE", "METODO", "OBJETO",
				"EXCEPCION", "PRIVADO" };
		return palabras[(int) (Math.random() * palabras.length)];
	}
}
