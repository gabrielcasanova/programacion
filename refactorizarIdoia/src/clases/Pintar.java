package clases;

public class Pintar {
	
	public static void pintarAhorcado(int fallos) {
		switch (fallos) {
		case 1:
			System.out.println("     ___");
			break;
		case 2:

			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("    ____ ");
			System.out.println("  |     |");
			System.out.println("  O     |");
			System.out.println(" -|-    |");
			System.out.println("       ___");
			break;
		case 7:
			System.out.println("   ____ ");
			System.out.println("  |     |");
			System.out.println("  O     |");
			System.out.println(" -|-    |");
			System.out.println("  A    ___");
			break;
		}
	}
	
	public static void pintarCaracteresVacios(char[][] caracteresPalabra) {
		System.out.println("####################################");
		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();
	}

}
