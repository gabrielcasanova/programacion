package clases;

public class Comprobar {
	
	public static int comprobarCaracteres(String caracteresElegidos, char [][] caracteresPalabra, int fallos) {
		for (int j = 0; j < caracteresElegidos.length(); j++) {
			boolean encontrado = false;
			for (int i = 0; i < caracteresPalabra[0].length; i++) {
				if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
					caracteresPalabra[1][i] = '1';
					encontrado = true;
				}
			}
			if (!encontrado)
				fallos++;
		}
		return fallos;

	}
	
	public static boolean comprobarGanar(char[][] caracteresPalabra, boolean acertado) {
		
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		return acertado;

	}
	

}
