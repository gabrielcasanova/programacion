package com.elenajif.sobrecarga;

public class Sobrecarga2 {

	public static void miTipo(int x) {
		System.out.println("Mi tipo (int) es "+x);
	}
	
	public static void miTipo(double x) {
		System.out.println("Mi tipo (double) es "+x);
	}
	
	public static void main (String args[]) {
		int i=10;
		double d=10.1;
		byte b=99;
		short s=10;
		float f=11.1F;
		
		Sobrecarga2.miTipo(i);//int
		Sobrecarga2.miTipo(d);//double
		Sobrecarga2.miTipo(b);//int
		Sobrecarga2.miTipo(s);//int
		Sobrecarga2.miTipo(f);//double				
		
	}
}
