package com.elenajif.sobrecarga;

public class Sobrecarga1 {
	public void demoSobrecarga() {
		System.out.println("M�todo sobrecargado sin par�metros");
	}
	
	public void demoSobrecarga(int a) {
		System.out.println("M�todo con un par�metro "+a);
	}
	
	public int demoSobrecarga(int a, int b) {
		System.out.println("M�todo con dos par�metros "+a +" y "+b);
		return a+b;
	}
	
	public double demoSobrecarga(double a, double b) {
		System.out.println("M�todo con dos par�metros "+a+" y "+b);
		return a+b;
	}
	
	public static void main (String args[]) {
		
		Sobrecarga1 objetoSobrecarga = new Sobrecarga1();
		//clase objeto = new constructor;
		
		System.out.println("M�todo 1");
		objetoSobrecarga.demoSobrecarga();
		
		System.out.println("M�todo 2");
		objetoSobrecarga.demoSobrecarga(3);
		
		System.out.println("M�todo 3");
		int x=objetoSobrecarga.demoSobrecarga(2,3);
		System.out.println(x);
		
		System.out.println("M�todo 4");
		double y=objetoSobrecarga.demoSobrecarga(4.2,3.4);
		System.out.println(y);
	}
}
