package com.elenajif.ejerciciosvectores;

import java.util.Scanner;

public class Ejercicio2Vectores {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Dame el tama�o del vector");
		int tam = input.nextInt();
		int num[] = new int[tam];

		rellenarNumAleatorioArray(num, 0, 9);
		mostrarArray(num);
		
		input.close();
	}

	public static void rellenarNumAleatorioArray(int lista[], int a, int b) {
		for (int i = 0; i < lista.length; i++) {
			// Generamos un n�mero entre los parametros pasados
			lista[i] = ((int) Math.floor(Math.random() * (a - b) + b));
		}
	}

	public static void mostrarArray(int lista[]) {
		for (int i = 0; i < lista.length; i++) {
			System.out.println("En el indice " + i + " esta el valor " + lista[i]);
		}
	}
}