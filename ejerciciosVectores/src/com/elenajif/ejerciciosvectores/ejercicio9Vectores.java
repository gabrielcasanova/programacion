package com.elenajif.ejerciciosvectores;

import java.util.Scanner;

public class Ejercicio9Vectores {
	public static void main(String[] args) {
		final int DIVISOR = 23;

		Scanner input = new Scanner(System.in);
		System.out.println("Dame los n�meros del DNI");
		int dni = input.nextInt();
		int resto = dni % DIVISOR;

		char letra = letraNif(resto);

		System.out.println("Tu letra de DNI es " + letra);

		input.close();
	}

	public static char letraNif(int res) {
		char letrasNif[] = { 'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V',
				'H', 'L', 'C', 'K', 'E' };
		return letrasNif[res];
	}
}
