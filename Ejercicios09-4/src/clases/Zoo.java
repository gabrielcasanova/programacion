package clases;

	/**
	 * Clase que actua como un almacen de animales
	 * @author DAW
	 *
	 */
public class Zoo {
	private Animal[] animales;
	
	/**
	 * constructor de un nuevo Zoo 
	 * @param maAnimales indica cual es la cantidad maxima de animales para este zoo
	 * 
	 */
	public Zoo(int maxAnimales) {
		this.animales = new Animal[maxAnimales];
	}
	
	/**
	 * da de alta un nuevo animal
	 * @param nombreAnimal que tendra el animal
	 * @param peso del animal
	 * @param especie del animal
	 * @param zoo del animal
	 */
	public void altaAnimal(String nombreAnimal, double peso, String especie, String zoo){
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] == null) {
				animales[i] = new Animal(nombreAnimal);
				animales[i].setPeso(peso);
				animales[i].setEspecie(especie);
				animales[i].setZoo(zoo);
				break;
			}
			
		}
		
	}
	
	/**
	 * busca un determinado animal dado un nombreAnimal
	 * @param nombreAnimal que se quiere buscar
	 * @return objeto Animal
	 */
	public Animal buscarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					return animales[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Elimina los animales con el codigo introducido
	 * @param nombreAnimal que se desea eliminar
	 */
	public void eliminarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i]=null;
				}
			}
		}
	}
	
	/**
	 * Pinta por consola todos los animales
	 */
	public void listarAnimales() {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
					System.out.println(animales[i]);
				
			}
		}
	}
	
	/**
	 * Cambia un nombreAnimal de un animal determinado
	 * @param nombreAnimal del que se quiere cambiar el animal
	 * @param nombreAnimal2 nuevo nombre
	 */
	public void cambiarNombreAnimal(String nombreAnimal, String nombreAnimal2) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i].setNombreAnimal(nombreAnimal2);
				}
			}
		}
	}
	
	/**
	 * pinta por consola una lista con los animales que contengan el zoo introducido
	 * @param nomberAnimal del que se quiere mostrar la lista de animales
	 */
	public void listarAnimalesPorZoo(String zoo) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getZoo().equals(zoo)) {
					System.out.println(animales[i]);
				}
			}
		}
	}
	
}
