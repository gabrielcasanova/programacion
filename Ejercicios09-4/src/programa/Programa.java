package programa;

import clases.Zoo;

public class Programa {

	public static void main(String[] args) {		
		System.out.println("1- Crear una instancia de Zoo llamada miZoo con 4 animales");
		int maxAnimales = 4;
		Zoo miZoo = new Zoo(maxAnimales);
		System.out.println("Instancia creada");

		System.out.println("\n2- Dar de alta 4 animales distintos.");
		System.out.println("Damos de alta 4 animales");
		miZoo.altaAnimal("Timón", 10, "Suricato", "zoo 1");
		miZoo.altaAnimal("Pumba", 30, "facóquero", "zoo 1");
		miZoo.altaAnimal("Simba", 60, "león", "zoo 1");
		miZoo.altaAnimal("Kowalski", 20, "Pingüino", "zoo 2");
		
		System.out.println("\n3- Listar los animales."); 
		miZoo.listarAnimales();
		
		System.out.println("\n4- Buscar un animal por su nombre y mostrar sus datos por pantalla.");
		System.out.println("Buscamos animal Timón");
		System.out.println(miZoo.buscarAnimal("Timón"));
		
		System.out.println("\n5- Eliminar un animal diferente al anterior.");
		System.out.println("Eliminamos el animal Simba");
		miZoo.eliminarAnimal("Simba");
		miZoo.listarAnimales();
		
		System.out.println("\n6- Almacenar un nuevo animal."); 
		System.out.println("Almacenamos Dori");
		miZoo.altaAnimal("Dori", 1, "pez cirujano", "zoo 2");
		miZoo.listarAnimales();
		
		System.out.println("\n7- Modificar el nombre de un animal.");
		System.out.println("Modificamos el animal Dori a Hola soy Dori");
		miZoo.cambiarNombreAnimal("Dori", "Hola soy Dori");
		miZoo.listarAnimales();

		System.out.println("\n8- Listar solo los animales de un zoo."); 
		System.out.println("Listamos los animales del zoo 1");
		miZoo.listarAnimalesPorZoo("zoo 1");
	}
}
