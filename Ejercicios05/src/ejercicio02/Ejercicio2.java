package ejercicio02;

public class Ejercicio2 {

	static final String MENSAJE_FIN = "Fin de programa";
	
	public static void main(String[] args) {
		
		int contador = 100;
		
		//Mostrandolos en una linea distinta
		while(contador > 0) {
			System.out.println(contador);
			contador--;
		}
		
		//Mostrándolos en la misma linea
		contador = 100;
		
		while(contador > 0) {
			System.out.print(contador);
			contador--;
		}
		
		System.out.println();
		System.out.println(MENSAJE_FIN);
	}

}
