package ejerciciosPruebaExcepciones;

public class Ejercicio1ConExcepciones {
	
	public static void main(String[] args) {
		//bloque try
		//incorpora el codigo regular de mi programa
		//el codigo que puede provocar la excepcion
		
		//el bloque catch
		//este es el bloque donde trataremos la excecpcion
		
		//bloque final
		//se ejecuta al final 
		//tanto si se ha provado la excepcion, como si no
		
		
		
	try {
		int i;
		int valor;
		i=3;
		valor = i/0;
		System.out.println(valor);
	//muestro la excepcion que quiero y le doy el valor que quiera
	}catch(ArithmeticException e) {
		//le dicimos que nos muestre el valor de la excepcion
		System.out.println(e.toString());
	}finally {
			System.out.println("Esto se imprime siempre");
		}
	}
	
	
	
	
	
	
	
}
