package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce un numero entero");
		int numero = input.nextInt();
		
		boolean condicion = numero % 2 == 0;
		
		if(condicion) { 
			System.out.println("Numero par");
		} else {
			System.out.println("N�mero impar");
		}
		
		input.close();
	}

}
