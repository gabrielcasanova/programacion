package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		System.out.println("Introduce una cadena");
		String cadena1 = escaner.nextLine();
		
		System.out.println("Introduce la segunda cadena");
		String cadena2 = escaner.nextLine();
		
		if(cadena1.equals(cadena2)) {
			System.out.println("Mismos caracteres - coincidencia completa");

		} else {
			
			if(cadena1.equalsIgnoreCase(cadena2)) {
				System.out.println("Mismas letras, aunque mayusculas y minusculas");
		
			} else {
				System.out.println("No son iguales");
			}	
		}
		
		
		escaner.close();
	}

}
