package ejer02;

import java.util.Scanner;

public class ejer02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Dame el num1");
		int num1 = input.nextInt();
		System.out.println("Dame el num2");
		int num2= input.nextInt();
		
		System.out.println("El maximo es: " + maximo(num1,num2));
		System.out.println("El minimo es: " + minimo(num1,num2));

	}

	
	public static int maximo(int num1, int num2) {
		int maximo = 0;
		
		if(num1>num2) {
			maximo=num1;
		}else {
			maximo=num2;
		}
		
		
		
		return maximo;
		
	}
	public static int minimo(int num1, int num2) {
		int minimo = 0;
		
		if(num1<num2) {
			minimo=num1;
		}else {
			minimo=num2;
		}
		
		
		
		return minimo;
		
	}
	
	
	
}
