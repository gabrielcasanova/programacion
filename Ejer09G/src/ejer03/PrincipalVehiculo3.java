package ejer03;

import java.util.Scanner;

import ejer03.Vehiculo3;

public class PrincipalVehiculo3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("tipo:");
		String tipo = input.nextLine();
		System.out.println("Marca");
		String marca = input.nextLine();
		System.out.println("Consumo");
		float consumo = input.nextFloat();
		System.out.println("N ruedas");
		int ruedas= input.nextInt();
		
		// intancio la clase, hay que importarla
		//creacion de objeto
		
		Vehiculo3 vehiculo1 = new Vehiculo3(tipo,marca,consumo,ruedas);
		
		//creo 2� vehiculo
		
		System.out.println("VAmos a por el 2� vehiculo");
		System.out.println("tipo:");
		tipo = input.nextLine();
		System.out.println("Marca");
		marca = input.nextLine();
		
		
		Vehiculo3 vehiculo2 =  new Vehiculo3(tipo, marca);
		
		//creo 3� sin nada
		
		
		Vehiculo3 vehiculo3 =  new Vehiculo3();
		
		
		
		System.out.println("VAmos a por el 3� vehiculo");
		System.out.println("tipo:");
		tipo = input.nextLine();
		
		vehiculo3.setTipo(tipo);
		
		System.out.println("Marca");
		marca = input.nextLine();
		vehiculo3.setMarca(marca);
		
		System.out.println("Consumo");
		consumo = input.nextFloat();
		vehiculo3.setConsumo(consumo);
		System.out.println("N ruedas");
		ruedas= input.nextInt();
		vehiculo3.setNumRuedas(ruedas);
		
		Vehiculo3.mostrarDatosVehiculo(vehiculo1);
		Vehiculo3.mostrarDatosVehiculo(vehiculo2);
		Vehiculo3.mostrarDatosVehiculo(vehiculo3);
		
		
		System.out.println("Cambio los kilometros");
		vehiculo1.trucarCuentaKm();
		vehiculo2.trucarCuentaKm(12000);
		
		
		
		

		
		
		
		
		
		

	}

	
	

	
}
