package ejer01;

public class Vehiculo {
	private String tipo;
	private String marca;
	private float consumo;
	private float kmTotales;
	private int numRuedas;
	private boolean funciona;
	
	
	//constructor sin argumentos
	
public Vehiculo() {
		
	}

//
public Vehiculo(String tipo, String marca) {
	// el this solo para los argumentos
	this.tipo=tipo;
	this.marca=marca;
	
	//estado inicial
	kmTotales=0;
	funciona=true;
	
}



	public Vehiculo(String tipo, String marca, float consumo, int numRuedas) {
	this.tipo = tipo;
	this.marca = marca;
	this.consumo = consumo;
	this.numRuedas = numRuedas;
	kmTotales=0;
	funciona=true;
}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public float getConsumo() {
		return consumo;
	}

	public void setConsumo(float consumo) {
		this.consumo = consumo;
	}


	public int getNumRuedas() {
		return numRuedas;
	}


	public void setNumRuedas(int numRuedas) {
		this.numRuedas = numRuedas;
	}

	public boolean isFunciona() {
		return funciona;
	}

	public void setFunciona(boolean funciona) {
		this.funciona = funciona;
	}
	
	public float getKmTotales() {
		return kmTotales;
	}

//Hata aqui settergetters defecto
	
	public float combustibleConsumido() {
		return (this.consumo*kmTotales)/100;	
	}
	
	public float combustibleConsumido(float kmTotales) {
		return (this.consumo*kmTotales)/100;	
	}
	
	public void trucarCuentaKm() {
		this.kmTotales=0;
		
	}
	
	public void trucarCuentaKm(float kmTotales) {
		this.kmTotales=0;
		
	}

	@Override
	public String toString() {
		return "Vehiculo [tipo=" + tipo + ", marca=" + marca + ", consumo=" + consumo + ", kmTotales=" + kmTotales
				+ ", numRuedas=" + numRuedas + ", funciona=" + funciona + "]";
	}
	
	


	public static void mostrarDatosVehiculo(Vehiculo vehiculo){
		
		System.out.println("\nTipo: " + vehiculo.getTipo());
		System.out.println("Marca: " + vehiculo.getMarca());
		System.out.println("Consumo: " + vehiculo.getConsumo());
		System.out.println("Numero de Ruedas: " + vehiculo.getNumRuedas());
		System.out.println("Funciona: " + vehiculo.isFunciona());
		System.out.println("Km: " + vehiculo.getKmTotales());
		System.out.println();
		
	}


	

}
