package ejercicios098;

import java.time.LocalDate;

public class Persona {

	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	private boolean esHombre;
	private double altura;
	private double peso;

	public Persona(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre) {
		// Utilizo el constructor anterior
		this(nombre, apellidos);
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		this.esHombre = esHombre;
	}

	public Persona(String nombre, String apellidos, String fechaNacimiento, boolean esHombre, double altura,
			double peso) {
		// Utilizo el constructor anterior
		this(nombre, apellidos, fechaNacimiento, esHombre);
		this.peso = peso;
		this.altura = altura;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public boolean isEsHombre() {
		return esHombre;
	}

	public void setEsHombre(boolean esHombre) {
		this.esHombre = esHombre;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

}
