package ejercicio13;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		int[][] matriz = new int[10][10];
		Scanner input = new Scanner(System.in);
		
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz[i].length; j++){
				matriz[i][j] = (int)(Math.random() * 50);
			}
		}
		
		System.out.println("Introduce el numero a buscar");
		int numero = input.nextInt();
		input.close();
		
		buscarNumero(matriz, numero);
	}

	private static void buscarNumero(int[][] matriz, int numero) {
		int contadorApariciones = 0;
		for(int i = 0; i < matriz.length; i++){
			for(int j = 0; j < matriz[i].length; j++){
				//Si la celda contiene al numero
				if(matriz[i][j] == numero){
					contadorApariciones++;
					System.out.print("["+i+"]["+j+"] ");
				}
			}
		}
		System.out.println("\nEl numero " + numero +" se ha encontrado " + contadorApariciones + " veces");
		
	}
	
}
