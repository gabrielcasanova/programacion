package ejercicio05;

import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] numeros = new int[5];
		
		for(int i = 0; i < numeros.length; i++){
			System.out.println("Introduce un numero");
			numeros[i] = input.nextInt();
		}
		
		mostrarEstadisticas(numeros);
		
		
		input.close();
	}

	static void mostrarEstadisticas(int[] numeros) {
		int mayor = 0;
		int menor = 0;
		int contadorPositivos = 0;
		int sumaPositivos = 0;
		int contadorNegativos = 0;
		int sumaNegativos = 0;
		
		
		for(int i = 0; i < numeros.length; i++){
			
			//Compruebo cuales son el mayor y menor
			if(i == 0){
				mayor = numeros[i];
				menor = numeros[i];
			}else if(numeros[i] > mayor){
				mayor = numeros[i];
			}else if(numeros[i] < menor){
				menor = numeros[i];
			}
			
			//Cuento los numeros y sumo sus valores
			if(numeros[i] >= 0){
				contadorPositivos++;
				sumaPositivos += numeros[i];
			}else{
				contadorNegativos++;
				sumaNegativos += numeros[i];
			}
			
		}
		//Muestro las estadisticas
		System.out.println("El mayor: " + mayor);
		System.out.println("El menor: " + menor);
		
		System.out.println("Media de positivos: " + ((double)sumaPositivos / contadorPositivos));
		System.out.println("Media de negativos : " + ((double)sumaNegativos / contadorNegativos));
		System.out.println("Media total: " + ((double)(sumaPositivos + sumaNegativos) / (numeros.length)));
	}

}
