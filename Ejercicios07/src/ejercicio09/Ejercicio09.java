package ejercicio09;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] enteros = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90};

		System.out.println("Introduce un entero");
		int num = input.nextInt();
		
		insertarOrdenado(enteros , num);
		
		for(int i = 0; i < enteros.length; i++){
			System.out.print(enteros[i] + " ");
		}
		
		
		input.close();
	}

	private static void insertarOrdenado(int[] enteros, int num) {
		
		for(int i = 0; i < enteros.length; i++){
			//Si el numero de la celda 
			//es mayor que el que he introducido
			//lo inserto en esa posicion
			if(enteros[i] > num){
				enteros[i] = num;
				return;
			}
		}
		
		//Si no lo he insertado, lo inserto en la ultima posicion
		enteros[enteros.length - 1] = num;
		
	}

}
