package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {
		
		int[] array = new int[10];
		
		for(int i = 0; i < array.length; i++){
			array[i] = (int)((Math.random() * 12) + 4);
		}

		mostrarArrayEnteros(array);
	}

	public static void mostrarArrayEnteros(int[] enteros){
		for(int i = 0; i < enteros.length; i++){
			System.out.println(enteros[i]);
		}
		
	}
}
