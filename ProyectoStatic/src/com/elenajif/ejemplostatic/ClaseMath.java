package com.elenajif.ejemplostatic;

public class ClaseMath {

    public static void main(String[] args) {
        System.out.println("Valor de PI");
        System.out.println(Math.PI);
        System.out.println("Potencia de 5 elevado a 3");
        System.out.println(Math.pow(5, 3));
        System.out.println("M�ximo de 6 y 9");
        System.out.println(Math.max(6,9));
    }    
}