package com.elenajif.ejemplostatic;

public class LecturaMath1 {

    public static void main(String[] args) {
      System.out.println("Vamos a usar la clase Math");
      System.out.println("Vamos a calcular una potencia");
      System.out.println("Dame la base");
      double base= Lectura.leerDouble();
      System.out.println("Dame el exponente");
      double exp= Lectura.leerDouble();
      System.out.println("El n�mero "+base+" elevado a "+exp+" es "+Math.pow(base, exp));
      System.out.println("Vamos a calcular el m�ximo de dos n�meros");
      System.out.println("Dame el primer numero");
      double n1= Lectura.leerDouble();
      System.out.println("Dame el segundo numero");
      double n2= Lectura.leerDouble();
      System.out.println("El m�ximo es "+Math.max(n1, n2));
      System.out.println("Vamos a calcular el m�nimo de dos n�meros");
      System.out.println("Dame el primer numero");
      double n3= Lectura.leerDouble();
      System.out.println("Dame el segundo numero");
      double n4= Lectura.leerDouble();
      System.out.println("El m�nimo es "+Math.min(n3, n4));
      System.out.println("Vamos a calcular la raiz cuadrada de un numero");
      System.out.println("Dame un numero");
      double n= Lectura.leerDouble();
      System.out.println("La ra�z es "+Math.sqrt(n));
    }
}