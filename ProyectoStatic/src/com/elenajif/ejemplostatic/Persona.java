package com.elenajif.ejemplostatic;

public class Persona {

    public String nombre;
    public int edad;
    private static int contador;

    public static int contadorPersonas() {
            return contador;
    }

    public static void incrementarContador() {
        contador++;
    }
    public static void main(String[] args) {
        Persona p1 = new Persona();
        p1.nombre="Pepe";
        p1.edad=23;
        System.out.println("Nombre persona1: "+p1.nombre);
        System.out.println("Nombre persona1: "+p1.edad);
        Persona.incrementarContador();
        Persona p2 = new Persona();
        p2.nombre="Mar�a";
        p2.edad=25;
        System.out.println("Nombre persona1: "+p2.nombre);
        System.out.println("Nombre persona1: "+p2.edad);
        Persona.incrementarContador();
        System.out.println("Se han creado: " + Persona.contadorPersonas() + " personas");
    }
}
