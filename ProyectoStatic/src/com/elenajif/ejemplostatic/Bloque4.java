package com.elenajif.ejemplostatic;

import java.util.Scanner;

public class Bloque4 {
	
		public static void mostrarValores(int a, int b) {
			System.out.println("Valor de a = " + a);
			System.out.println("Valor de b = " + b);
		}
		public static void main(String[] args) {
			Scanner lectura = new Scanner(System.in);
			System.out.println("Dame un n�mero");
			int x=lectura.nextInt();
			System.out.println("Dame otro n�mero");
			int y=lectura.nextInt();
			Bloque4.mostrarValores(x, y);
			lectura.close();
		}
	}
