package com.elenajif.ejemplostatic;

import java.util.Scanner;
public class Lectura {  
	static Scanner leer = new Scanner(System.in);
    
    public static int leerEntero(){
        System.out.println("Dame un numero entero:");
        int n= leer.nextInt();
        return n;
        }
    public static double leerDouble(){
        System.out.println("Dame un numero double:");
        double n= leer.nextDouble();
        return n;
    }
}