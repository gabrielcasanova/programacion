package com.elenajif.ejemplostatic;

public class Bloque3 {
	
		public static void mostrarValores(int a, int b) {
			System.out.println("Valor de a = " + a);
			System.out.println("Valor de b = " + b);
		}
		public static void main(String[] args) {
			int x=10;
			int y=20;
			Bloque3.mostrarValores(x, y);
		}
	}
