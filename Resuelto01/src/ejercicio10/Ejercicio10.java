package ejercicio10;

public class Ejercicio10 {

	public static void main(String[] args) {

		int variableA = 100;
		
		System.out.println( variableA % 5 == 0 ? "Es multiplo de 5" : "No lo es");
		System.out.println( variableA % 10 == 0 ? "Es multiplo de 10" : "No lo es");
		System.out.println(variableA >= 100 ? (variableA == 0 ? "Es igual a 100" : "Es mayor que 100"): "Es menor a 100");
		
 
	}

}
