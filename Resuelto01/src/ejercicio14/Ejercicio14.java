package ejercicio14;

public class Ejercicio14 {

	public static void main(String[] args) {
		
		String textoEntero = "2345";
		String textoEnteroLargo = "2151346235634564576346";
		String textoDecimal = "1234.5646";
		String textoByte = "123";

		int entero = Integer.parseInt(textoEntero);
		long enteroLargo = Long.parseLong(textoEnteroLargo);
		double decimal = Double.parseDouble(textoDecimal);
		byte numByte = Byte.parseByte(textoByte);
		
		
	}

}
