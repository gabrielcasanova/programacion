package com.elenajif.clasearraylist;

import java.util.ArrayList;

public class ArrayList1 {
	public static void main(String[] args) {
		System.out.println("Creamos ArrayList");
		ArrayList<String> nombres = new ArrayList<String>();
		System.out.println("A�adimos");
		nombres.add("Ana");
		nombres.add("Luisa");
		nombres.add("Felipe");
		System.out.println("Mostramos");
		System.out.println(nombres); // [Ana, Luisa, Felipe]
		System.out.println("A�adimos uno m�s");
		nombres.add(1, "Pablo");
		System.out.println("Mostramos");
		System.out.println(nombres); // [Ana, Pablo, Luisa, Felipe]
		System.out.println("Borramos el 0");
		nombres.remove(0);
		System.out.println("Mostramos");
		System.out.println(nombres); // [Pablo, Luisa, Felipe]
		System.out.println("Ponemos en el 0 nombre Alfoso");
		nombres.set(0, "Alfonso");
		System.out.println("Mostramos");
		System.out.println(nombres); // [Alfonso, Luisa, Felipe]
		System.out.println("Quitamos ultimo");
		String s = nombres.get(1);
		String ultimo = nombres.get(nombres.size() - 1);
		System.out.println("Mostramos");
		System.out.println(s + " " + ultimo); // Luisa Felipe
	}
}
