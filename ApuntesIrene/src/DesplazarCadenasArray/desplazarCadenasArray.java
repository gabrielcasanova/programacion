package DesplazarCadenasArray;

public class desplazarCadenasArray {
	public static void desplazar(String[] array, int desplazamiento) {
		desplazamiento = desplazamiento % array.length;

		// El siguiente bucle se repite tantas veces como el desplazamiento
		for (int i = 0; i < desplazamiento; i++) {
			String aux;
			aux = array[array.length - 1];
			// empiezo desde el final del array
			// moviendo el penultimo elemento a la casilla del 1
			for (int j = array.length - 1; j > 0; j--) {
				array[j] = array[j - 1];
			}
			array[0] = aux;
		}
	}
	
	
	private static String[] desplazar2(String[] array, int desplazamiento) {
		//desplazamiento = desplazamiento % cadenas.length;
		String[] cadenas2 = new String[array.length];
		
		//Mientras la casilla del array se desplace a otra mas adelante
		//Simplemente copio el valor
		//Si necesito moverlas al principio del array, calculo su posicion
		for(int i = 0; i < array.length; i++){
			if(desplazamiento + i > array.length -1){
				cadenas2[i + desplazamiento - array.length] = array[i];
			}else{
				cadenas2[i + desplazamiento] = array[i];
			}
		}
		
		return cadenas2;
	}

}
