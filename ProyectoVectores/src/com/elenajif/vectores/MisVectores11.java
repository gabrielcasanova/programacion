package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores11 {
	public static void main(String args[]) {
		int sumaPar=0;
		int sumaImpar=0;
		int[] numeros = new int[5];
		Scanner teclado = new Scanner(System.in);
		
		//carga del vector
		for (int i=0;i<numeros.length;i++) {
			System.out.println("Introduce un n�mero entero");
			numeros[i]=teclado.nextInt();
		}
		//comprobamos si es par/impar y contamos
		for (int i=0;i<numeros.length;i++) {
			if(numeros[i]%2==0) {
				sumaPar=sumaPar+numeros[i];
			} else {
				sumaImpar=sumaImpar+numeros[i];
			}
		}
		System.out.println("El n�mero de pares es "+sumaPar);
		System.out.println("El n�mero de impares es "+sumaImpar);
		teclado.close();
		
	}
}
