package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores7 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Introduce la longitud del Array");
		int longVector = input.nextInt();
		int miVector[] = new int[longVector];

		for (int i = 0; i < longVector; i++) {
			System.out.println("Introduce el componente n" + i);
			miVector[i] = input.nextInt();
		}
		int opcion = 0;
		do {
			System.out.println("Selecciona una opcion:");
			System.out.println("1.- Suma del Array");
			System.out.println("2.- Minimo del Array");
			System.out.println("3.- Maximo del Array");
			System.out.println("4.- Media del Array");
			System.out.println("5.- Salir");
			opcion = input.nextInt();
			switch (opcion) {
			case 1:
				int suma = 0;
				for (int i = 0; i < longVector; i++) {
					suma += miVector[i];
				}
				System.out.println("La suma es: " + suma);
				break;

			case 2:
				int minimo = miVector[0];
				for (int i = 0; i < longVector; i++) {
					if (miVector[i] < minimo)
						minimo = miVector[i];
				}
				System.out.println("El minimo es: " + minimo);
				break;

			case 3:
				int maximo = miVector[0];
				for (int i = 0; i < longVector; i++) {
					if (miVector[i] > maximo)
						maximo = miVector[i];
				}
				System.out.println("El maximo es: " + maximo);
				break;

			case 4:
				int media = 0;
				suma = 0;
				for (int i = 0; i < longVector; i++) {
					suma += miVector[i];
				}
				media = suma / longVector;
				System.out.println("La media es: " + media);
				break;

			case 5:
				System.exit(0);
				break;

			default:
				System.out.println("Opcion no valida");
			}
		} while (opcion != 5);
		input.close();
	}
}
