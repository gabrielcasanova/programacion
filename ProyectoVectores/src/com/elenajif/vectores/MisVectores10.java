package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores10 {
	public static void main(String args[]) {
		int contPar=0;
		int contImpar=0;
		int[] numeros = new int[30];
		Scanner teclado = new Scanner(System.in);
		
		//carga del vector
		for (int i=0;i<numeros.length;i++) {
			System.out.println("Introduce un n�mero entero");
			numeros[i]=teclado.nextInt();
		}
		//comprobamos si es par/impar y contamos
		for (int i=0;i<numeros.length;i++) {
			if(numeros[i]%2==0) {
				contPar=contPar+1;
			} else {
				contImpar=contImpar+1;
			}
		}
		System.out.println("El n�mero de pares es "+contPar);
		System.out.println("El n�mero de impares es "+contImpar);
		teclado.close();
		
	}
}
