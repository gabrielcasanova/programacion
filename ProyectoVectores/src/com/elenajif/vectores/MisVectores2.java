package com.elenajif.vectores;

import java.util.Scanner;

public class MisVectores2 {

	public static void main(String[] args) {
		// declarar vector de String
		String miVector[] = new String[3];
		//declaramos un scanner
		Scanner lectura = new Scanner(System.in);
		//pedimos los datos
		for (int i=0;i<3;i++) {
			System.out.println("Dame la componente "+i+" del vector");
			miVector[i]=lectura.next();
		}
		//mostrar los datos
		for (int i=0;i<3;i++) {
			System.out.println("La componente "+i+" del vector es: "+miVector[i]);
		}
		lectura.close();
	}

}
