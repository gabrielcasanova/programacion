package programa;

import clases.Ebook;
import clases.TelefonoMovil;
import clases.Terminal;

public class Programa {

	public static void main(String[] args) {
		
		System.out.println("Creo objeto de superClase");
		System.out.println("------------------------");
		Terminal miTerminal = new Terminal("3310", "nokia", 200, 2005, "negro");
		System.out.println("Muestro datos de terminal");
		System.out.println(miTerminal);
		System.out.println("Hacemos descuento del 10 % y cambio color a azul");
		miTerminal.descuento(10);
		miTerminal.cambiaColor("azul");
		
		System.out.println(miTerminal);
		System.out.println("------------------------");
		
		
		
		System.out.println("Creo objeto de clase Telefono");
		TelefonoMovil miTelefono= new TelefonoMovil("s10+", "Samsung", 699, 2020, "Rojo", 128, 8);
		System.out.println(miTelefono);
		
		System.out.println("Instalo Sistema Operativo");
		miTelefono.instalarSO(20);
		System.out.println(miTelefono);
		
		System.out.println("Formateo el telefono, cambio de color a Verde y le hago descuento del 35%");
		System.out.println();
		miTelefono.formatear();
		miTelefono.cambiaColor("Verde");
		miTelefono.descuento(35);
		System.out.println(miTelefono);
		
		System.out.println("Creo objeto clase Ebook");
		Ebook miebook = new Ebook("paperwhite", "amazon", 99, 2019, "blanco");
		System.out.println(miebook);
		System.out.println("Metemos 150 libros y cambiamos el estado de la luz integrada");
		System.out.println("Descuento del 79% y cambiamos el color a rojo");
		miebook.sumaLibros(150);
		miebook.cambiaLuzIntegrada();
		miebook.descuento(79);
		miebook.cambiaColor("rojo");
		System.out.println(miebook);
		
		
	}

}
