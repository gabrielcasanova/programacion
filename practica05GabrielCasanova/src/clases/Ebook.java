package clases;

public class Ebook extends Terminal{
	
	
	private int numeroLibros;
	private boolean luzIntegrada;
	
	
	public Ebook(String modelo, String marca, double precio, int anoSalida, String color) {
		super(modelo, marca, precio, anoSalida, color);
		this.numeroLibros=0;
		this.luzIntegrada=false;
		
	}
	public Ebook(String modelo) {
		super(modelo);
		
	}
	
	public String toString() {
		return super.toString() 
				+"Numero de libros:" + numeroLibros + "\nTiene luz integrada:" + luzIntegrada + "\n---------------------";
	}
	
	//metodos
	public void sumaLibros(int libros) {
		if(libros<1) {
			System.out.println("Valor erroneo");
		}else {
			numeroLibros+=libros;
		}
		
	}
	public void cambiaLuzIntegrada() {
		if(luzIntegrada==true) {
			luzIntegrada=false;	
		}else if (luzIntegrada==false) {
			luzIntegrada=true;
		}
	}
	
	
	public int getNumeroLibros() {
		return numeroLibros;
	}
	public void setNumeroLibros(int numeroLibros) {
		this.numeroLibros = numeroLibros;
	}
	public boolean isLuzIntegrada() {
		return luzIntegrada;
	}
	public void setLuzIntegrada(boolean luzIntegrada) {
		this.luzIntegrada = luzIntegrada;
	}

	
	

}
