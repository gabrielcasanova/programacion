package clases;

public class Terminal {
	
	
	private String modelo;
	private String marca;
	private double precio;
	private int anoSalida;
	private String color;
	
	
	
	
	//contructor
	public Terminal(String modelo) {
		this.modelo = modelo;
		this.anoSalida=2020;
		this.precio=100;
		this.marca= "Sin marca";
		this.color= "Negro";
		
	}
	

	public Terminal(String modelo, String marca, double precio, int anoSalida, String color) {
		this.modelo = modelo;
		this.marca = marca;
		this.precio = precio;
		this.anoSalida = anoSalida;
		this.color = color;
	}


	
	
	
	
	//Metodo toString
	public String toString() {
		return "Modelo: " + modelo + 
				"\nMarca: " + marca + 
				"\nPrecio: " + precio + " euros " +
				"\nA�o de salida: " + anoSalida+ 
				"\nEl color es: " + color + " \n";
	}

	//Metodos
	public void descuento(double porcentaje) {
		double descuento =  precio * porcentaje/100;
		precio-=descuento;
	}
	
	public void cambiaColor(String color) {
		this.color=color;
	}
	
	
	
	//Setters y getters
	
	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public int getAnoSalida() {
		return anoSalida;
	}


	public void setAnoSalida(int anoSalida) {
		this.anoSalida = anoSalida;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	
	
	
	
	
	
	
	

}
