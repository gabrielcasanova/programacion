package clases;

public class TelefonoMovil extends Terminal  {
	
	private double memoriaTotal;
	private double memoriaLibre;
	private double versionActual;
	private double versionInicial;
	
	
	
	
	
	public TelefonoMovil(String modelo, String marca, double precio, int anoSalida, String color, double memoriaTotal, double version) {
		super(modelo, marca, precio, anoSalida, color);
		this.memoriaTotal=memoriaTotal;
		this.memoriaLibre=memoriaTotal;
		this.versionActual=version;
		this.versionInicial=version;
	}

	public TelefonoMovil(String modelo) {
		super(modelo);
		// TODO Auto-generated constructor stub
	}


	
	public String toString() {
		return super.toString() 
				+ "Capacidad total: " + memoriaTotal + " GB \n"
						+ "Capacidad restante: " + memoriaLibre + " GB \nVersion: " + versionActual+"\n---------------------"
				;
	}


	
	//metodos
	public void instalarSO(double tamano) {
		memoriaLibre=memoriaTotal-tamano;
		versionActual++;
		
	}
	
	public void formatear() {
		this.memoriaLibre=memoriaTotal;
		this.versionActual=versionInicial;
		
	}
	
	
	
	public double getMemoriaTotal() {
		return memoriaTotal;
	}
	public void setMemoriaTotal(int memoriaTotal) {
		this.memoriaTotal = memoriaTotal;
	}
	public double getMemoriaLibre() {
		return memoriaLibre;
	}
	public void setMemoriaLibre(int memoriaLibre) {
		this.memoriaLibre = memoriaLibre;
	}
	public double getVersion() {
		return versionActual;
	}
	public void setVersion(int version) {
		this.versionActual = version;
	}

	public void setMemoriaTotal(double memoriaTotal) {
		this.memoriaTotal = memoriaTotal;
	}

	public void setMemoriaLibre(double memoriaLibre) {
		this.memoriaLibre = memoriaLibre;
	}

	public void setVersion(double version) {
		this.versionActual = version;
	}










	
	


	

}
