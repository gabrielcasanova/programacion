package com.elenajif.ejercicios0925;

public class ProfesorInterino extends Profesor {

	int anyosInterino;

	public ProfesorInterino() {

	}

	public ProfesorInterino(String nombre, String apellidos, int edad, String ciclo, int anyosInterino) {
		super(nombre, apellidos, edad, ciclo);
		this.anyosInterino = anyosInterino;
	}

	public int getAnyosInterino() {
		return anyosInterino;
	}

	public void setAnyosCargo(int anyosInterino) {
		this.anyosInterino = anyosInterino;
	}

	public String toString() {
		return "Nombre y apellidos " + nombre + " " + apellidos + "\nEdad " + edad + "\nCiclo " + ciclo
				+ "\nA�os Interino " + anyosInterino;
	}
	
	public double obtenerSalario() {
		double salario=1100+this.anyosInterino*45.23;
		return salario;
	}

}
