package programa;

import clases.GestorTienda;

public class Programa {

	public static void main(String[] args) {
		
		System.out.println("1- Activamos el gestor");
		GestorTienda gestor = new GestorTienda();
		System.out.println();
		System.out.println("2- Compramos varios telefonos (metodo alta clase 1)");
		gestor.comprarTelefono("1111", "Samung", "S10", 699);
		gestor.comprarTelefono("1112", "Nokia", "3310", 99);
		gestor.comprarTelefono("1113", "iPhone", "X", 999);
		System.out.println("Comprobamos la lista (metodo listar clase 1)");
		gestor.listarTelefonos();
		//gestor.comprarTelefono("1114", "Xiaomi", "a10", 449);
		//gestor.comprarTelefono("1115", "mate30", "Huawei", 849);
		System.out.println();
		System.out.println("Intentamos comprar otro con el codigo 1111(comprobacion altaclase1)");
		gestor.comprarTelefono("1111", "LG", "Note", 199);
		
		System.out.println();
		
		System.out.println("3- Buscamos telefono con codigo 1112");
		System.out.println(gestor.buscarTelefono("1112"));
		
		System.out.println();
		
		System.out.println("4-Eliminamos telefono con codigo 1113 y listamos");
		
		gestor.eliminarTelefono("1113");
		gestor.listarTelefonos();
		
		System.out.println();
			
		System.out.println("5-Doy de alta 3 tiendas (clase2)");
		gestor.altaTienda("Tienda 1", 10000);
		gestor.altaTienda("Tienda 2", 15000);
		gestor.altaTienda("Tienda 3", 25000);
	
		System.out.println("Compruebo la lista");
		gestor.listarTiendas();
		System.out.println();
		
		System.out.println("6- Busco la tienda 2 ");
		System.out.println(gestor.buscarTienda("Tienda 2"));
		
		System.out.println();
		
		System.out.println("7- Elimino la tienda 3 y listo");
		gestor.eliminarTienda("Tienda 3");
		gestor.listarTiendas();
		System.out.println();
		
		System.out.println("8- Listo tienda por nombre");
		gestor.listarTiendaNombre("Tienda 1");
		System.out.println();
		
		System.out.println("10- Asigno telefono a tienda (clase 1 a clase2) y listo");
		gestor.enviarTelefonoATienda("1111", "Tienda 1");
		gestor.enviarTelefonoATienda("1112", "Tienda 2");
		gestor.listarTiendas();
		System.out.println();
		System.out.println("9- Listo tiendas por marca de telefono ");
		
		System.out.println(gestor.buscarTiendaMarca("Nokia"));
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
