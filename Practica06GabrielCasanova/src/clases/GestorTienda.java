package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
//1
public class GestorTienda {
	private ArrayList<Telefono> listaTelefonos;
	private ArrayList<Tienda> listaTiendas;
	
//2	
	public GestorTienda() {
		listaTelefonos = new ArrayList<Telefono>();
		listaTiendas = new ArrayList<Tienda>();
	}
	
	public boolean existeTelefono(String codigo) {
		for(Telefono telefono: listaTelefonos) {
			if(telefono!=null&& telefono.getCodigo().equalsIgnoreCase(codigo)) {
				return true;
			}
		}
		return false;
	}
	
//3	//metodo alta clase1
	public void comprarTelefono(String codigo, String modelo, String marca, double precio) {
		if(!existeTelefono(codigo)) {
			Telefono nuevoTelefono = new Telefono(codigo, modelo, marca,precio);
			nuevoTelefono.setFechaCompra(LocalDate.now());
			listaTelefonos.add(nuevoTelefono);
			
			
		}else {System.out.println("Ya has comprado ese telefono");
		}
		
	}
	
//4	//metodo listar clase1
	public void listarTelefonos() {
		for(Telefono listaTelefonos : listaTelefonos) {
			if(listaTelefonos!=null) {
				System.out.println(listaTelefonos);
			}
		}
	}
	
	
//5	// metodo buscar elemento clase 1
	public Telefono buscarTelefono(String codigo) {
		for(Telefono telefono:listaTelefonos) {
			if(telefono!=null&&telefono.getCodigo().equalsIgnoreCase(codigo)) {
				return telefono;
			}
		}
		return  null;
	}
	
//6	//metodo eliminar clase1
	public  void eliminarTelefono(String codigo) {
		Iterator<Telefono> iteratorTelefonos = listaTelefonos.iterator();
		while(iteratorTelefonos.hasNext()) {
			Telefono telefono= iteratorTelefonos.next();
			if(telefono.getCodigo().equalsIgnoreCase(codigo)) {
				iteratorTelefonos.remove();
			}
		}
	}
	//Metodo existe clase 2
	public boolean existeTienda(String nombre) {
		for(Tienda tienda: listaTiendas) {
			if(tienda!=null&& tienda.getNombre().equalsIgnoreCase(nombre)) {
				return true;
			}
		}
		return false;
	}
		
		
//7	//metodo alta clase 2
	
	public void altaTienda(String nombre, double presupuesto) {
		if(!existeTienda(nombre)) {
			Tienda nuevaTienda = new Tienda(nombre, presupuesto);
			listaTiendas.add(nuevaTienda);
			
			
		}else {System.out.println("Esta tienda ya existe");
		}
		
	}
	
//8 //eliminar clase 2	
	public void eliminarTienda(String nombre) {
		Iterator<Tienda> iteratorTienda = listaTiendas.iterator();
		
		while (iteratorTienda.hasNext()) {
			Tienda tienda = iteratorTienda.next();
			if(tienda.getNombre().equalsIgnoreCase(nombre)) {
				iteratorTienda.remove();
			}
		}
	}
	
		
//9	// metodo buscar elemento clase 2
		public Tienda buscarTienda(String nombre) {
			for(Tienda tienda:listaTiendas) {
				if(tienda!=null&&tienda.getNombre().equalsIgnoreCase(nombre)) {
					return tienda;
				}
			}
			return  null;
		}
//10 //metodo listar clase 2 por atributo clase 2
		
		public void listarTiendaNombre(String nombre) {
			for (Tienda tienda :listaTiendas) {
				if(tienda.getNombre().equals(nombre)) {
					System.out.println(tienda);
				}
			}
		}
		
		
		
//11	metodo listar clase clas2 con elemento clase 1
		
		public void listarTiendaMarca(String marca) {
			for (Tienda tienda : listaTiendas) {
				if(tienda.getTelefono().getMarca().equals(marca)) {
					System.out.println(tienda);
				}
			}
		}
		
		
		
		
		
		//metodo buscar clase2 con elemento clase 1
		public Tienda buscarTiendaMarca(String marca) {
			for(Tienda tienda:listaTiendas) {
				if(tienda!=null && tienda.getTelefono().getMarca().equals(marca))
					return tienda;
				
			}
			return  null;
		
		
		}
		
		
		
		
		
			
		public void listarTiendas() {
			for(int i = 0; i< listaTiendas.size();i++) {
				System.out.println(listaTiendas.get(i));
			}
			
		}
		
		
		
	
	
//12//metodo asigna elemento clase1 a clase2
	

		public  void  enviarTelefonoATienda(String codigo, String nombre) {
			
				if(buscarTelefono(codigo)!=null && buscarTienda(nombre)!=null) {
					Telefono telefono = buscarTelefono(codigo);
					Tienda tienda = buscarTienda(nombre);
					tienda.setTelefono(telefono);
					tienda.setUnidades(+1);
				
				
				
			}
		}
		
	
	
		
		
		
		
		
			

	

}
