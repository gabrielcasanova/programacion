package clases;

import java.time.LocalDate;

public class Telefono {
	private String codigo;
	private String modelo;
	private String marca;
	private double precio;
	private LocalDate fechaCompra;
	
	
	
	
	public Telefono(String codigo) {
		this.codigo = codigo;
	}
	public Telefono(String codigo, String marca, String modelo, double precio) {
		this.codigo=codigo;
		this.modelo = modelo;
		this.marca = marca;
		this.precio = precio;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public LocalDate getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(LocalDate fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	@Override
	public String toString() {
		return "Telefono [codigo=" + codigo + ", modelo=" + modelo + ", marca=" + marca + ", precio=" + precio
				+ ", fechaCompra=" + fechaCompra + "]";
	}
	
	

}
