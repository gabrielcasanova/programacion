package clases;

public class Tienda {
	private String nombre;
	private double presupuesto;
	private int unidades;
	private Telefono telefono;
	
	
	public Tienda() {
		
	}
	public Tienda(String nombre, double presupuesto) {
		this.nombre = nombre;
		this.presupuesto = presupuesto;
		this.unidades=0;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPresupuesto() {
		return presupuesto;
	}
	public void setPresupuesto(double presupuesto) {
		this.presupuesto = presupuesto;
	}
	public int getUnidades() {
		return unidades;
	}
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	public Telefono getTelefono() {
		return telefono;
	}
	public void setTelefono(Telefono telefono) {
		this.telefono = telefono;
	}
	@Override
	public String toString() {
		return "Tienda [nombre=" + nombre + ", presupuesto=" + presupuesto + ", unidades=" + unidades + ", telefono="
				+ telefono;
	}
	

}
