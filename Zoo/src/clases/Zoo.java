package clases;

public class Zoo {

	private Animal[] animales;

	// UN UNICO CONSTRUCTOR INICIALIZA EL ARRAY ANIMALES
	public Zoo(int maxAnimales) {
		this.animales = new Animal[maxAnimales];
	}

	// Metodo void altaAnimal(StringnombreAnimal, doublepeso, Stringespecie,
	// Stringzoo)
	public void altaAnimal(String nombreAnimal, double peso, String especie, String zoo) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] == null) {
				animales[i] = new Animal(nombreAnimal);
				animales[i].setEspecie(especie);
				animales[i].setPeso(peso);
				animales[i].setZoo(zoo);
				break;
			}
		}
	}

	//para probar
	public void imprimirUno() {
		System.out.println(animales[2]);
	}
	// m�todo AnimalbuscarAnimal(StringnombreAnimal).Devuelveel objeto Animalcuyo
	// campo nombreAnimales igual al valor recibidocomo par�metro. Si no existe
	// unAnimalcon ese nombre devolver� null.

	public Animal buscarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					return animales[i];
				}
			}
		}
		return null;
	}

	//m�todovoid eliminarAnimal(StringnombreAnimal). 
	// Debe buscar en el Array,el animalque corresponde con el c�digo del par�metro
	// recibido, y eliminarlode esa posici�n del array.Si no existe un animalcon
	// dicho c�digo, no har� nada
	
	public void eliminarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if(animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i] = null;
				}
			}
		}
	}

	// m�todovoid listarAnimales().Muestra por consola todos los datosde los
	// animalesque tengo en el Array
	
	public void listarAnimales() {
		for (int i = 0; i < animales.length; i++) {
			System.out.println(animales[i]);
		}
	}

	// m�todovoid listarAnimalPorZoo(Stringzoo).Funciona igual que el m�todo
	// listarAnimales, pero lista solo los animalesde un zooque recibe comopar�metro
	
	public void listarAnimalPorZoo(String zoo) {
		for (int i = 0; i < animales.length; i++) {
			if(animales[i].getZoo().equals(zoo)) {
				System.out.println(animales[i]);
			}
		}
	}
	
	/**
	 * Cambia un nombreAnimal de un animal determinado
	 * @param nombreAnimal del que se quiere cambiar el animal
	 * @param nombreAnimal2 nuevo nombre
	 */
	
	public void cambiarNombreAnimal(String nombreAnimal, String nombreAnimal2) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i].setNombreAnimal(nombreAnimal2);
				}
			}
		}
	}
	// GETTERS AND SETTERS
	public Animal[] getAnimales() {
		return animales;
	}

	public void setAnimales(Animal[] animales) {
		this.animales = animales;
	}

}