package clases;

public class Animal {

	private String nombreAnimal; 
	private double peso;
	private String especie;
	private String zoo;
	
	
	//UN UNICO CONSTRUCTOR QUE RECIBE STRING NOMBREANIMAL E INICIALIZA ESE ATRIBUTO
	public Animal(String nombreAnimal) {
		this.nombreAnimal = nombreAnimal;
	}
	
	//METODO TOSTRING 
	public String toString() {
		return "El nombre del animal es: " + nombreAnimal 
				+ " \n Su peso es: " + peso 
				+ " \n Su especie es: " + especie 
				+ " \n Su zoo es: " + zoo + "\n";
	}
	
	//GETTERS AND SETTERS
	
	public String getNombreAnimal() {
		return nombreAnimal;
	}
	public void setNombreAnimal(String nombreAnimal) {
		this.nombreAnimal = nombreAnimal;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public String getEspecie() {
		return especie;
	}
	public void setEspecie(String especie) {
		this.especie = especie;
	}
	public String getZoo() {
		return zoo;
	}
	public void setZoo(String zoo) {
		this.zoo = zoo;
	}
}
