package programa;

import clases.Zoo;

public class Programa {

	public static void main(String[] args) {
		System.out.println("Creamos instancia");
		int maxAnimales = 4;
		Zoo abZoo = new Zoo(maxAnimales);
		System.out.println("Instancia creada");
	
		System.out.println("Damos d alta 4, 3 con el mismo zoo");
		abZoo.altaAnimal("mono", 65, "sapiens", "zaragoza");
	
		abZoo.altaAnimal("jirafa", 65, "euskadi", "zaragoza");
		abZoo.altaAnimal("leon", 65, "mamifero", "zaragoza");
		abZoo.altaAnimal("elefante", 65, "pez", "cadiz");
		
		System.out.println("Listamos animales");
		abZoo.listarAnimales();
	
		System.out.println("Buscamos animal por nombre");
		System.out.println("Mostramos sus datos por pantalla");
		System.out.println(abZoo.buscarAnimal("mono"));
		
		System.out.println("Eliminamos animal diferente a mono");
		abZoo.eliminarAnimal("jirafa");
		
		System.out.println("Listamos animales para ver si se ha eliminado");
		abZoo.listarAnimales();
		
		System.out.println("Almacenamos un nuevo animal");
		abZoo.altaAnimal("jirafa2", 65, "sapiens", "zaragoza");
		System.out.println("Listamos");
		abZoo.listarAnimales();
		
		System.out.println("Modificamos su nombre");
		abZoo.cambiarNombreAnimal("jirafa2", "jirafa3");
		System.out.println("Listamos para ver si se ha modificado");
		abZoo.listarAnimales();
		
		
		System.out.println("Listamos por zoo");
		abZoo.listarAnimalPorZoo("zaragoza");
	}

}
