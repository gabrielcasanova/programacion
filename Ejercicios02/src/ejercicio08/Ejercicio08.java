package ejercicio08;

import java.util.Scanner;

public class Ejercicio08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		
		System.out.println("Escribe un numero de 5 cifras");
		
		int numero = input.nextInt();
		System.out.println(numero);
		System.out.println(numero/10000);
		System.out.println(numero/1000);
		System.out.println(numero/100);
		System.out.println(numero/10);
		System.out.println(numero);
		
		System.out.println("Introduce otro numero de 5 cifras");
		
		String numero2 = input.next();
		
		System.out.println(numero2.substring(0, 1));
		System.out.println(numero2.substring(0, 2));
		System.out.println(numero2.substring(0, 3));
		System.out.println(numero2.substring(0, 4));
		System.out.println(numero2.substring(0, 5));
	
		
		
		input.close();
		
		
		
		
		
	
		
		
		
	}

}
