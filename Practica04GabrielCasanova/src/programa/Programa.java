package programa;

import clases.Terminales;

public class Programa {
	public static void main(String[] args) {
		
		System.out.println("Instanciamos la clase");
		
		//Esto es el tama�o de la lista de terminales (tama�o del array)
		int cantidad = 4;
		
		Terminales ListaTerminales = new Terminales(cantidad);
		ListaTerminales.altaTerminal("S20+", "Samsung", 699, 2019, true);
		ListaTerminales.altaTerminal("3310", "Nokia", 59, 2002, false);
		ListaTerminales.altaTerminal("mi9", "Xiaomi", 259, 2019, false);
		ListaTerminales.altaTerminal("XS", "iPhone", 699, 2020, true);
		
		System.out.println("Lista de los terminales");
		ListaTerminales.listarTerminales();
		
		System.out.println("Vamos a buscar la marca Samsung");
		
		System.out.println(ListaTerminales.buscarTerminal("samsung"));
		
		System.out.println("Eliminamos el terminal modelo XS");
		
		ListaTerminales.eliminarTerminal("Xs");
		
		System.out.println("Volvemos a listar los terminales");
		
		ListaTerminales.listarTerminales();
		
		System.out.println("Introducimos un nuevo terminal");
		ListaTerminales.altaTerminal("p30", "Huawei", 799, 2018, true);
		
		ListaTerminales.listarTerminales();
		
		System.out.println("Modifico el nombre del modelo. Busco p30 y establezco p30 pro edition");
		
		ListaTerminales.cambiarModelo("p30", "p30 pro edition");
		
		ListaTerminales.listarTerminales();
		
		System.out.println("Listo los terminales que tienen 5G");
		
		ListaTerminales.listar5G(true);
		
		
		
		
		
		
		
	}

}
