package clases;

public class Terminales {
	
	private Terminal [] terminales;

	
	
	//Constructor que inicial el array de la lista de los terminales
	//El int que le paso es el tama�o del array
	public Terminales(int cantidad) {
		
		this.terminales = new Terminal[cantidad];
	}
	
	
	//Metodo alta terminal
	//Esto busca cuando esta vacio y establece los datos que le pasamos
	
	public void altaTerminal(String modelo, String marca, double precio
			, int anoSalida,boolean cincoG) {
		// Se mete el break para que salte a la siguiente instruccion, si no meteria
		//la misma informacion en todos los sectores
		for (int i= 0; i < terminales.length;i++) {
			if(terminales[i]==null) {
				terminales[i]= new Terminal(modelo);
				terminales[i].setMarca(marca);
				terminales[i].setPrecio(precio);
				terminales[i].setAnoSalida(anoSalida);
				terminales[i].setCincoG(cincoG);
				break;	
				
			}	
			
		}
		
	}
	//Imprime todos los index
	public void listarTerminales() {	
		for(int i= 0; i < terminales.length;i++) {
			System.out.println(terminales[i]);
		}
	}
	
	
	// Busca en todos los datos si hay un atributo que coincide con lo que le pasamos
	//y lo devuelve para posteriormente imprimirlo
	public Terminal buscarTerminal(String marca) {
		for( int i = 0; i < terminales.length;i++) {
			if(terminales[i]!=null) {
				if(terminales[i].getMarca().equalsIgnoreCase(marca)) {
					return terminales[i];
				}
			}
		}	
		return null;
	}
	//Busca en todos los datos y si hay un atributo que coincide con lo que le pasamos
	// lo establece entero como null, que es lo mismo que borrarlo
	public void eliminarTerminal(String modelo) {
		for( int i = 0; i < terminales.length;i++) {
			if(terminales[i]!= null) {
				if(terminales[i].getModelo().equalsIgnoreCase(modelo)) {
					terminales[i]=null;
				}
			}
			
		}
	}
	//Busca entre todos los datos y le condicionamos que si encuentra un atributo 
	//como el atributo 1 (getter), le establecera el atributo 2 (setter)
	public void cambiarModelo(String modelo, String modeloNuevo) {
		
		for(int i = 0; i < terminales.length; i++) {
			if(terminales!=null) {
				if(terminales[i].getModelo().equalsIgnoreCase(modelo)) {
					terminales[i].setModelo(modeloNuevo);
				}
			}
		}
		
	}
	//busca entre todos los datos y nos imprime la condicion que le decimos
	public void listar5G(boolean cincoG) {
		for(int i = 0; i<terminales.length;i++) {
			
			if(terminales[i].isCincoG()==cincoG) {
				System.out.println(terminales[i]);
			}
		}
		
	}
	
	
	

}
