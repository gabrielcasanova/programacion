package clases;

public class Terminal {
	
	
	private String modelo;
	private String marca;
	private double precio;
	private int anoSalida;
	private boolean cincoG;
	
	
	//contructor
	public Terminal(String modelo) {
		this.modelo = modelo;
		
	}


	//Metodo toString
	public String toString() {
		return "El modelo= " + modelo + 
				"\nMarca=" + marca + 
				"\nPrecio=" + precio + 
				"\nA�o de salida=" + anoSalida+ 
				"\nTiene 5G =" + cincoG + " \n";
	}

	//Setters y getters
	
	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public int getAnoSalida() {
		return anoSalida;
	}


	public void setAnoSalida(int anoSalida) {
		this.anoSalida = anoSalida;
	}


	public boolean isCincoG() {
		return cincoG;
	}


	public void setCincoG(boolean cincoG) {
		this.cincoG = cincoG;
	}
	
	
	
	
	
	
	
	

}
