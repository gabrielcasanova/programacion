public class Ejercicio7 {
  public static void main(String args[]) { 
    Scanner sc=new Scanner(System.in);  
    System.out.println("Dame un numero entero");
    int b=sc.nextInt();
    System.out.println("Dame otro numero, con decimales, (double)");
    double a=sc.nextDouble();    
    double exp= 1;
    int n = 0;
    
    for (n= b; n>0; n--){
        exp = exp*a;
    }
    System.out.println("El resultado es " + exp);
    } 
}
