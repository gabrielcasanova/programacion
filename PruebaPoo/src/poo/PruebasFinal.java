package poo;

public class PruebasFinal {

	public static void main(String[] args) {
		
		
		Empleados trabajador1 = new Empleados("Paco");
		Empleados trabajador2 = new Empleados("Ana");
		Empleados trabajador3 = new Empleados("Antonio");
		Empleados trabajador4 = new Empleados("Maria");
		trabajador1.cambiaSeccion("RRHH");
	
		System.out.println(trabajador1.devuelveDato() + "\n"
				+ trabajador2.devuelveDato()+"\n" + trabajador3.devuelveDato()+"\n"+
				trabajador4.devuelveDato());
		System.out.println(Empleados.dameIDSiguiente());
		
		
		

	}

}





class Empleados{
	
	
	
	public Empleados(String nom) {
		
		nombre=nom;
		ID=IDSiguiente;
		IDSiguiente++;
		
		
		seccion="Administracion";
		
		
	}
	
	public void cambiaSeccion (String seccion) {//setter
		
		
		this.seccion = seccion;
		
	}
	

	public String devuelveDato() {
		
		return "El nombre es: "+ nombre + ", la seccion es: "+ seccion + " y el ID es= " + ID;
	}
	
	public static String dameIDSiguiente() {
		
		return "El IDSiguiente es: " + IDSiguiente;
	}
	
	
	private  final String nombre;
	
	private String seccion;
	
	private int ID;
	
	private static int IDSiguiente=1;
}