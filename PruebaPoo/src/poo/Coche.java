package poo;

public class Coche {
	
	//declaro las variables
	//Private es para encapsular, que no se pueda modificar por otro lado
	
	//Variables ENCAPSULADAS
	//Carcateristicas comunes
	private  int ruedas;
	
	private int largo;
	
	private int ancho;
	
	private int motor;
	
	private int peso_plataforma;
	
	//Caracteristicas variables
	
	private  String color;
	
	private int peso_total;
	
	private boolean asientos_cuero, climatizador;
	
	//CONSTRUCTOR
	public Coche() {
		ruedas = 4;
		largo = 2000;
		ancho = 300;
		motor= 1600;
		peso_plataforma=500;
	}
	
	//METODO
	//Como queremos que sea una frase, ponemos string + nombreMetodo
	//getter, siempre llevan return
	public String dime_datos_generales() {
		
		return "La plataforma del vehiculo tiene " +ruedas + " ruedas"
				+ ". Mide "+ largo/1000 + " metros con un ancho de "+ ancho +
				"cm y un peso de plataforma de "+ peso_plataforma + " kg" ;
	}
	
	//Setter// Establece el dato
	public void establece_color(String color_coche) {
		color=color_coche;
	}
	
	public String dime_color() {
		
		return "El color del coche es " +  color;
	}
	
	/*public, para poder acceder desde el main
	 * void, porque es un setter
	 * configura_asientos: es el nombre del metodo
	 * String porque vamos a recibir una variable de tipo string
	 * asientos_cuero es el argumento
	 * this.referenciaClase, es el operador para hacer referencia a esa clase
	 * Funcionamiento: Si al setter le pasamos si, al boolean
	 * le ponemos true, si no, false
	 * */
	public void configura_asientos(String asientos_cuero) {
		
		if(asientos_cuero.equalsIgnoreCase("si")) {
			this.asientos_cuero=true;
		}else {   
			this.asientos_cuero=false;
		}
		
	}
	//getter
	public String dime_asientos() {
		if(asientos_cuero) {// es lo mismo que asientos_cuero==true, lo que pasa que se abrevia
			
			return "El coche tiene asientos de cuero";
		}else {
			return "El coche tiene asientos de serie";
		}
	}
	
	public void configura_climatizador(String climatizador) {
		if (climatizador.equalsIgnoreCase("si")){
			this.climatizador=true;
		}else {
			this.climatizador=false;
		}
		
	}
	
	public String dime_climatizador() {
		if(climatizador==true) {
			return "El coche incorpora climatizador";
		}else {
			return "El coche lleva aire acondicionado";
		}
	
		}
	
	
	public String dime_peso_coche(){
		int peso_carroceria = 500;
		peso_total=peso_plataforma+peso_carroceria;
		if(asientos_cuero==true) {
			peso_total=peso_total+50;
		}else {
			peso_total=peso_total+20;
		}
		return "El peso del coche es " + peso_total;
		
	}
	
	public int precio_coche() {
		int precio_final=10000;
		
		if(asientos_cuero==true) {
			precio_final+=2000;
		}
		if(climatizador==true) {
			precio_final+=1500;
		}
		return precio_final;
	}
	
}
