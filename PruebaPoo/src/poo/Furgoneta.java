package poo;
//la palabra extends convierte a Coche enn superclase
//padre>coche--hijo>furgoneta
//no admite herencia a multiples clases
public class Furgoneta extends Coche{
	
	private int capacidadCarga;
	
	private int plazasExtra;
	
	public Furgoneta(int plazasExtra, int capacidadCarga) {
		super(); //llamar al contructor de la clase padre
		this.capacidadCarga=capacidadCarga;
		this.plazasExtra=plazasExtra;
		
	}
	
	public String dimeDatosFurgoneta() {
		return "La capacidad de carga es: " + capacidadCarga + " Y  las plazas son: " + plazasExtra;
	}

}
