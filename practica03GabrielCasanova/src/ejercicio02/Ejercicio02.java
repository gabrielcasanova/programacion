package ejercicio02;

public class Ejercicio02 {

	public static void main(String[] args) {

		int[] diaMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		
		mostrarDiaMes(diaMes);
				
		

	}
	
	
	public static void mostrarDiaMes(int[] diaMes) {


		for(int i=0;i<diaMes.length;i++) {
			System.out.println("El mes "+ (i+1) + " tiene " + diaMes[i]+" dias");
		}
	}

}
