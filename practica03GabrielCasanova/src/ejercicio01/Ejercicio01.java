package ejercicio01;

import java.util.Scanner;

public class Ejercicio01 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el mes del que desee consultar sus d�as");
		
		String mes = input.nextLine();
		
		mostrarDiaMes(mes);
		
		
		input.close();
	}
	
	public static void mostrarDiaMes(String mes) {
		
		if (mes.equalsIgnoreCase("enero") || mes.equalsIgnoreCase("marzo") || mes.equalsIgnoreCase("mayo") || mes.equalsIgnoreCase("julio") 
				|| mes.equalsIgnoreCase("agosto")  || mes.equalsIgnoreCase("octubre") || mes.equalsIgnoreCase("noviembre")) {

			System.out.println("El mes " + mes + " tiene 31 dias");

		} else if (mes.equalsIgnoreCase("abril") || mes.equalsIgnoreCase("junio") || mes.equalsIgnoreCase("septiembre")
				|| mes.equalsIgnoreCase("noviembre")) {

			System.out.println("El mes " + mes + " tiene 30 dias");
		} else if (mes.equalsIgnoreCase("febrero")) { 
		
			System.out.println("El mes " + mes + " tiene 28 dias");
		}else {
			System.out.println("No has introducido un valor correcto");
		}

	}

}