package ejercicio03;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		String [] vector = {"oveja", "caballo","perro","gato","raton","leon","vaca","conejo","gallina"};
		
		System.out.println("Escribe el nombre de un animal. \n"
				+ "Vamos a comprobar si lo tenemos registrado");
		
		Scanner input = new Scanner(System.in);
		String palabra = input.next();
		
		if(comprobarPalabra(vector,palabra)==true) {
			System.out.println("La palabra esta en el vector");
		}else {
			System.out.println("La palabra NO esta en el vector");
		}
		
		input.close();

	}
	
	
	public static boolean comprobarPalabra (String [] vector, String palabra) {
		boolean condicion =  false;
		for ( int i = 0; i < vector.length;i++) {
			if(palabra.equalsIgnoreCase(vector[i])) {
				condicion=true;
			
			}
		}return condicion;
		
		
	}

}
