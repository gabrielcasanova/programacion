package com.elenajif.cuentasbancarias;

public class CuentaCorriente extends Cuenta {
	// constructores
	public CuentaCorriente() {
		super();
	}

	public CuentaCorriente(String numero, String titular, double saldo, double interes) {
		super(numero, titular, saldo, interes);
	}
	//metodos
	public double reintegro(int perras) {
		saldo-=perras;
		return saldo;
	}
}
