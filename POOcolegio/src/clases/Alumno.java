package clases;

import java.time.LocalDate;

public class Alumno {
	
	
	private String codAlumno;
	private String nombre;
	
	private String apellidos;
	private String codProfesor;
	private LocalDate fechaMatricula;
	
//unico contructor
	public Alumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}


	public String getCodAlumno() {
		return codAlumno;
	}


	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public String getCodProfesor() {
		return codProfesor;
	}


	public void setCodProfesor(String codProfesor) {
		this.codProfesor = codProfesor;
	}


	public LocalDate getFechaMatricula() {
		return fechaMatricula;
	}


	public void setFechaMatricula(LocalDate fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}


	@Override
	public String toString() {
		return "Codigo Alumno=" + codAlumno
				+ "\nNombre=" + nombre 
				+ "\nApellidos=" + apellidos 
				+ "\nCodigo Profesor="+ codProfesor 
				+ "\nFecha Matricula=" + fechaMatricula + "\n";
	}
	
	
	
}

