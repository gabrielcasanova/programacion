package clases;
import java.time.LocalDate;



public class Profesor {
	
	// Despues de hacer la primera clase, hay que declararla en otro
	//Cogemos el mismo nombre de la clase anterior y declaramos el array
	
	private Alumno [] alumnoArray;
	
	//Para contruir el objeto profesor le damos la cantidad
	//el array es igual al construcor de la clase anterior
	public Profesor(int cantidad) {
		alumnoArray= new Alumno[cantidad];
	}
	
	//Aqui le decimos que si encuentra uno vacio, que contruya
	//una clase alumno
	
	public void altaAlumno(String codAlumno, String nombre, String apellidos, String codProfesor) {
		for(int i = 0; i< alumnoArray.length;i++) {
			if(alumnoArray[i]==null) {
				alumnoArray[i] = new Alumno(codAlumno);
				alumnoArray[i].setNombre(nombre);
				alumnoArray[i].setApellidos(apellidos);
				alumnoArray[i].setCodProfesor(codProfesor);
				alumnoArray[i].setFechaMatricula(LocalDate.now());
				break;
				
			}
		}
	}
	
	
	public void listarAlumnos() {
		for(int i = 0; i< alumnoArray.length;i++) {
			System.out.println(alumnoArray[i]);
		}
	}
	
	public void eliminarAlumno(String codAlumno) {
		for(int i = 0; i< alumnoArray.length;i++) {
			if(alumnoArray[i]!=null) {
				if(alumnoArray[i].getCodAlumno().equals(codAlumno)) {
					alumnoArray[i]=null;
				}
			}
		}
		
	}
	public void buscarAlumno(String codAlumno) {
		for(int i = 0; i< alumnoArray.length;i++) {
			if(alumnoArray[i]!=null) {
				if(alumnoArray[i].getCodAlumno().equals(codAlumno)) {
					System.out.println(alumnoArray[i]);
				}
			}
		}
		
	}
	
	public void cambiarNombre(String nombre, String nombre2) {
		for(int i = 0; i< alumnoArray.length;i++) {
			if(alumnoArray[i]!=null) {
				if(alumnoArray[i].getNombre().equalsIgnoreCase(nombre)) {
					alumnoArray[i].setNombre(nombre2);
				}
			}
			
			
			
		}
	}
	
	
	
	
	
	

}
