package programa;


import clases.Profesor;

public class Programa {
	
	public static void main(String[] args) {
		System.out.println("Instanciamos la clase");
		
		int cantidad = 4;
	
		Profesor profe1 = new Profesor(cantidad);
		
		profe1.altaAlumno("1000", "Gabriel", "Casanova", "2000");
		profe1.altaAlumno("1001", "Paco", "Jones", "2000");
		
		profe1.altaAlumno("1002", "Ana", "Bohueles", "3000");
		profe1.altaAlumno("1003", "Jonhy", "Melabo", "3000");
		
		profe1.listarAlumnos();
		System.out.println("elimino alumno con codigo 1002");
		profe1.eliminarAlumno("1002");
		profe1.listarAlumnos();
		profe1.altaAlumno("1004", "Maria", "Unpajote", "2000");
		profe1.listarAlumnos();
		System.out.println("Busco alumno con codigo 1000");
		profe1.buscarAlumno("1000");
		
		
		
		
	}

}
