package ejercicios;

import clases.ListaPersonas;
import clases.Persona;

public class Ejercicio11 {

	public static void main(String[] args) {

		ListaPersonas miLista = new ListaPersonas();
		
		miLista.add(null);
		miLista.add(1, new Persona("Fer" , "1234"));
		miLista.add(1, new Persona("Fran", "1234"));
		miLista.add(1, new Persona(null, null));
		miLista.add(1, new Persona("Silvia", "2346"));
		
		for(Persona persona : miLista){
			System.out.println(persona);
		}
		
		System.out.println("\nToString de ArrayList");
		System.out.println(miLista);

	}

}
