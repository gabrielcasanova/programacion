package ejercicios;

import clases.SubClase;
import clases.SuperClase;

public class Ejercicio01 {

	public static void main(String[] args) {
		
		SuperClase padre = new SuperClase("Fernando");
		SubClase hijo = new SubClase("Fernando" ,5);
		
		System.out.println(padre);
		System.out.println(hijo);
		
	}

}
