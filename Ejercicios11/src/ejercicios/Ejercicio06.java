package ejercicios;

import clases.Avion;
import clases.Barco;
import clases.Coche;
import clases.Vehiculo;

public class Ejercicio06 {

	public static void main(String[] args) {
		Barco barco = new Barco("1234", "Zodiac", 7, "Juanjo", false);
		System.out.println("BARCO");
		System.out.println(barco);
		
		
		
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println(barco.getCantidadInstancias());
		
		Avion avion = new Avion("1351AF", "Boeing", 2, 2);
		System.out.println("\nAVION");
		System.out.println(avion);
		
		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println(avion.getCantidadInstancias());
		
		Coche coche = new Coche("FDS-1234", "Peugeot", 5, 120.7);
		System.out.println("\nCOCHE");
		System.out.println(coche);

		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println(coche.getCantidadInstancias());
		
		Vehiculo vehiculo = new Vehiculo("2346", "honda", 3);
		System.out.println("\nVEHICULO");
		System.out.println(vehiculo);

		System.out.println(Vehiculo.getCantidadInstanciasStatic());
		System.out.println(vehiculo.getCantidadInstancias());
		
		

		
	}

}
