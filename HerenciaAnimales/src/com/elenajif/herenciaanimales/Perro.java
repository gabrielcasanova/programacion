package com.elenajif.herenciaanimales;

public class Perro extends Mamifero{
	public void dormir() {
		System.out.println("El perro duerme");
	}

	public void ladrar() {
		System.out.println("El perro ladra");
	}

	public void grunir() {
		System.out.println("El perro gru�e");
	}
}
