package com.elenajif.ejercicios092;

import java.util.Scanner;

public class Alumno {

	String nombre;
	String apellidos;
	double notaMedia;

	static Scanner in = new Scanner(System.in);
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public double getNotaMedia() {
		return notaMedia;
	}

	public void setNotaMedia(double notaMedia) {
		this.notaMedia = notaMedia;
	}

	public Alumno() {
		nombre = "";
		apellidos = "";
		notaMedia = 0.0;
	}

	public boolean Promociona(double notaMedia) {
		if (notaMedia >= 5) {
			return true;
		}
		return false;
	}

	public void rellenarAlumno() {
		System.out.println("Dime el nombre");
		nombre = in.nextLine();
		this.setNombre(nombre);
		System.out.println("Dime los apellidos");
		apellidos = in.nextLine();
		this.setApellidos(apellidos);
		System.out.println("Dime la nota media");
		notaMedia = in.nextDouble();
		this.setNotaMedia(notaMedia);
		in.nextLine();
	}

	public void visualizarAlumno(Alumno alumno) {
		System.out.println("El nombre es " + nombre);
		System.out.println("El apellido es " + apellidos);
		System.out.println("La nota media es " + notaMedia);
	}

}