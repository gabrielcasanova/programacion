package com.elenajif.ejercicios093;

public class Numero {

	private int numero;

	public Numero() {
		this.numero = 0;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getNumero() {
		return numero;
	}

	public boolean primo() {
		for (int i = 2; i < numero; i++) {
			if (numero % i == 0) {
				return false;
			}
		}
		return true;
	}

	public long factorial() {
		long factorial = 1;
		for (int i = 1; i <= numero; i++) {
			factorial *= i;
		}
		return factorial;
	}

	public void piramide1() {
		for (int i = 1; i <= numero; i++) {
			for (int j = 1; j <= numero; j++) {
				if (i <= j) {
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}

	public void piramide2() {
		for (int i = numero; i >= 1; i--) {
			for (int j = numero; j >= 1; j--) {
				if (i <= j) {
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}

}
