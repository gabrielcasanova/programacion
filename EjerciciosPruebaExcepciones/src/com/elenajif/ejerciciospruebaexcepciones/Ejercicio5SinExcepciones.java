package com.elenajif.ejerciciospruebaexcepciones;

import java.util.Scanner;

public class Ejercicio5SinExcepciones {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Dame un n�mero");
		double x = input.nextDouble();
		System.out.println("Raiz cuadrada de " + x + " = " + Math.sqrt(x));
		input.close();

	}

}
