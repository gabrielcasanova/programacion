package com.elenajif.ejerciciospruebaexcepciones;

public class Ejercicio4ConExcepciones {

	public static void main(String[] args) {
		try {
			int n = Integer.parseInt("M");
			System.out.println(n);
		} catch (NumberFormatException e) {
			System.err.println("Las cadenas no se pueden convertir a int \n");
		} finally {
			System.out.println("La has vuelto a liar");
		}
	}

}
