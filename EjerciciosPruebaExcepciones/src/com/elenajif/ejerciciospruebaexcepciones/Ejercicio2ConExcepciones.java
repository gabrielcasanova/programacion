package com.elenajif.ejerciciospruebaexcepciones;

public class Ejercicio2ConExcepciones {

	public static void main(String[] args) {
		String cadenas[] = { "cadena1", "cadena2", "cadena3", "cadena4" };
		try {
			for (int i = 0; i <= 4; i++) {
				System.out.println(cadenas[i]);
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Error, fuera del indice del array");
		} finally {
			System.out.println("Esto se imprime siempre");
		}

	}

}
