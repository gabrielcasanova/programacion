package com.elenajif.ejerciciospruebaexcepciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio7ThrowExcepciones {

	public static void main(String[] args) throws IOException  {
			lectura();
	}

	public static void lectura() throws IOException {
		BufferedReader br2= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Dame un frase");
		String frase = br2.readLine();
		System.out.println("La frase es "+frase);
	}
}
