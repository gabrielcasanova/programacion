package ejercicio17;

public class Ejercicio17 {

	public static void main(String[] args) {

		String cadena1 = "n";
		String cadena2 = "Otra cadena";
		
		int resultado = cadena1.compareTo(cadena2);
		// resultado > 0 -> cadena1 > cadena2
		//resultado < 0 -> cadena1 < cadena2
		//resultado == 0 -> son iguales
		
		System.out.println(resultado > 0 ? cadena1 + 
				" es mayor que " + 
				cadena2 : (resultado == 0 ? cadena1 
						+ " es igual a " + cadena2 : 
							cadena1 + " es menor a " + cadena2));

	}

}
