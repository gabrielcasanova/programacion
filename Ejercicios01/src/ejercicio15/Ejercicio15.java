package ejercicio15;

public class Ejercicio15 {

	static final String MENSAJE_INICIO = "inicia el programa";
	static final String MENSAJE_FIN = "fin del programa";
	
	public static void main(String[] args) {
		
		String cadenaLarga = "Una cadena larga de mas de 20 caracteres";
		
		//Obtengo un subcadena desde el caracter 10 hasta el 17
		String subCadena = cadenaLarga.substring(10, 17);
		System.out.println(subCadena);
		
		
	}

}
