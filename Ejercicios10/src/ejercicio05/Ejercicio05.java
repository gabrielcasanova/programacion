package ejercicio05;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio05 {

	public static void main(String[] args) {
		ArrayList<Integer> lista =  new ArrayList<>();
		
		for(int i = 0; i < 100; i++){
			lista.add((int)(Math.random() * 20) + 1);
		}
		
		/*
		 * Muestro el contenido
		 */
		Iterator<Integer> iterador = lista.iterator();
		while(iterador.hasNext()){
			System.out.println(iterador.next());
		}	
		
		/*
		 * Eliminando con la clase Iterator
		 */
		iterador = lista.iterator();
		while(iterador.hasNext()){
			Integer numero = iterador.next();
			if(numero >= 10 && numero <= 15){
				iterador.remove();
			}
		}
		
		/*
		 * Vuelvo a listar
		 */
		iterador = lista.iterator();
		while(iterador.hasNext()){
			System.out.println(iterador.next());
		}	

	}

}
