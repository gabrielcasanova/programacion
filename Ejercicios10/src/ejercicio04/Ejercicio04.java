package ejercicio04;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio04 {

	public static void main(String[] args) {
		ArrayList<Integer> lista =  new ArrayList<>();
		
		for(int i = 0; i < 100; i++){
			lista.add((int)(Math.random() * 20) + 1);
		}
		
		for(int numero : lista){
			System.out.print(numero + " ");
		}
		
		
		/*
		 * Eliminando con un for en sentido descendente
		 */
		for(int i = lista.size() - 1; i >= 0; i--){
			if(lista.get(i) >= 10 && lista.get(i) <= 15){
				lista.remove(i);
			}
		}
		
		/*
		 * Eliminando con un bucle while
		 */
		int contador = 0;
		while(contador < lista.size()){
			if(lista.get(contador) <= 15 &&
					lista.get(contador) >= 10){
				lista.remove(contador);
			}else{
				contador ++;
			}
		}
		
		/*
		 * Eliminando con la clase Iterator
		 */
		Iterator<Integer> it = lista.iterator();
		while(it.hasNext()){
			Integer numero = it.next();
			if(numero >= 10 && numero <= 15){
				it.remove();
			}
		}
		
		
	}

}
