package ejercicio07;

import java.util.ArrayList;
import java.util.Iterator;

public class Ejercicio07 {

	public static void main(String[] args){
		ArrayList<String> cadenas = new ArrayList<>();
		cadenas.add("hola");
		cadenas.add("adios");
		cadenas.add("hola");
		cadenas.add("hola");
		cadenas.add("adios");
		
		borrarCadenas("hola", cadenas);
		
		for(String cadena : cadenas){
			System.out.println(cadena);
		}
	}

	private static void borrarCadenas(String cadenaABorrar, ArrayList<String> cadenas) {
		Iterator<String> iterador = cadenas.iterator();
		
		while(iterador.hasNext()){
			String cadena = iterador.next();
			if(cadena.equals(cadenaABorrar)){
				iterador.remove();
			}
		}
	}
}
