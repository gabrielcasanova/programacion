package ejercicio17;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce dia 1");
		int dia = input.nextInt();
		
		System.out.println("Introduce mes 1");
		int mes = input.nextInt();
		
		System.out.println("Introduce anno 1");
		int anno = input.nextInt();
		
		System.out.println("Introduce dia 2");
		int dia2 = input.nextInt();
		
		System.out.println("Introduce mes 2");
		int mes2 = input.nextInt();
		
		System.out.println("Introduce anno 2");
		int anno2 = input.nextInt();
		
		
		//Paso las dos fechas a numero de dias
		int diasFecha1 = dia + mes * 30 + anno * 360;
		int diasFecha2 =  dia2 + mes2 * 30 + anno2 * 360;
		
		//Calculo la diferencia de dias
		int diferencia = diasFecha1 - diasFecha2;
		
		//Si una la primera fecha es anterior a la segunda
		//Saldra una cantidad negativa, por lo que invierto el signo
		if( diferencia < 0){
			diferencia = - diferencia;
		}
		
		System.out.println("diferencia de dias: " + diferencia);
		
		input.close();

	}

}
